# Testing...
$LOAD_PATH << 'test'

# Run the Camping test apps.
require "test/test_helper"
Dir['test/app_*.rb'].each { |f| require f }
